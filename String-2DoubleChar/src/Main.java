public class Main {


    /*
    Given a string, return a string where for every char in the original, there are two chars.
    link: http://codingbat.com/prob/p165312
    */

    public static void main(String[] args) {

        Main main = new Main();
        main.showAnswer("");
        main.showAnswer("Hi-There");
        main.showAnswer("Word!");
        main.showAnswer("a");
        main.showAnswer("aa");
    }

    private String doubleChar(String str) {

        StringBuilder newStr = new StringBuilder("");

        for (int i = 0; i < str.length(); i++) {
            newStr.append(str.charAt(i));
            newStr.append(str.charAt(i));
        }
        return newStr.toString();
    }

    private void showAnswer(String str) {
        System.out.println("Previous string: " + str + " | New string: " + doubleChar(str));
    }
}
