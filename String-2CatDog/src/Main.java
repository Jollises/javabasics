public class Main {

    /*
    Return true if the string "cat" and "dog" appear the same number of times in the given string.
    link: http://codingbat.com/prob/p111624
    */
    public static void main(String[] args) {
        Main main = new Main();
        main.showAnswer("");
        main.showAnswer("catxxdogxxxdog");
        main.showAnswer("dogdogcat");
        main.showAnswer("dog");
        main.showAnswer("cat");
    }

    private boolean catDog(String str) {

        int catCount = 0;
        int dogCount = 0;

        for (int i = 0; i < str.length() - 2; i++) {
            if (str.substring(i, i + 3).equals("cat")) {
                catCount++;
            }
            if (str.substring(i, i + 3).equals("dog")) {
                dogCount++;
            }
        }
        return dogCount == catCount;
    }

    private void showAnswer(String str) {

        if (catDog(str)) {
            System.out.println(str + " | same count of dog and cats ");
        } else {
            System.out.println(str + " | not the same count of dog and cats ");
        }

    }
}
