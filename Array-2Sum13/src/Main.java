import java.util.Arrays;

public class Main {

    /*
    Return the sum of the numbers in the array, returning 0 for an empty array. Except the number 13 is very unlucky,
    so it does not count and numbers that come immediately after a 13 also do not count.
    link: http://codingbat.com/prob/p127384
    */
    public static void main(String[] args) {

        int[] arr1 = {};
        int[] arr2 = {13, 1, 2, 13, 2, 1, 13};
        int[] arr3 = {13, 13};
        int[] arr4 = {13, 0, 13};
        int[] arr5 = {5, 7, 2};

        Main main = new Main();

        main.showAnswer(arr1);
        main.showAnswer(arr2);
        main.showAnswer(arr3);
        main.showAnswer(arr4);
        main.showAnswer(arr5);
    }

    private int sum13(int[] nums) {

        int sum = 0;

        for (int i = 0; i < nums.length; i++) {

            if (nums[i] != 13) {
                sum += nums[i];
            } else {
                ++i; // jump over the number that goes after 13
            }
        }
        return sum;
    }

    private void showAnswer(int[] array) {

        System.out.println(Arrays.toString(array) + " sum of numbers without 13 and his right neighbor if it exist is " +
                sum13(array));
    }


}
