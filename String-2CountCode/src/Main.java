public class Main {

    /*
    Return the number of times that the string "code" appears anywhere in the given string, except we'll accept any
    letter for the 'd', so "cope" and "cooe" count.
    link: http://codingbat.com/prob/p123614
    */
    public static void main(String[] args) {
        Main main = new Main();
        main.showAnswer("");
        main.showAnswer("cozexxcope");
        main.showAnswer("cozcop");
        main.showAnswer("AAcodeBBcoleCCccoreDD");
        main.showAnswer("aaacodebbb");
    }

    private int countCode(String str) {

        int count = 0;

        for (int i = 0; i < str.length() - 3; i++) {
            if (str.substring(i, i + 2).equals("co") && str.charAt(i + 3) == 'e') {
                count++;
            }
        }
        return count;
    }

    private void showAnswer(String str) {
        System.out.println(str + " | \"code\" appears " + countCode(str) + " times");
    }
}
