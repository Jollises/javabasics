import java.util.Arrays;

public class Main {


    /*
    Return the sum of the numbers in the array, except ignore sections of numbers starting with a 6 and extending to
    the next 7 (every 6 will be followed by at least one 7). Return 0 for no numbers.
    link: http://codingbat.com/prob/p111327
    */
    public static void main(String[] args) {

        int[] arr1 = {};
        int[] arr2 = {1, 2, 2, 6, 99, 99, 7};
        int[] arr3 = {1, 6, 2, 6, 2, 7, 1, 6, 99, 99, 7};
        int[] arr4 = {6, 8, 1, 6, 7};
        int[] arr5 = {6, 7, 11};

        Main main = new Main();
        main.showAnswer(arr1);
        main.showAnswer(arr2);
        main.showAnswer(arr3);
        main.showAnswer(arr4);
        main.showAnswer(arr5);
    }

    private int sum67(int[] nums) {

        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 6) {

                while (nums[i] != 7) { //while i in range of 6 to 7 do nothing
                    i++;
                }
            } else {
                sum += nums[i];
            }
        }
        return sum;
    }

    private void showAnswer(int[] array) {

        System.out.println(Arrays.toString(array) + " sum of number out of the range of 6 to 7 inclusive is " +
                sum67(array));
    }
}
