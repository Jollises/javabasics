import java.util.Arrays;

public class Main {

    /*Return the number of even ints in the given array. Note: the % "mod" operator computes the remainder, e.g. 5 % 2
     is 1.
     link: http://codingbat.com/prob/p162010*/

    public static void main(String[] args) {

        int[] arr1 = {};
        int[] arr2 = {2, 1, 2, 3, 4};
        int[] arr3 = {2, 2, 0};
        int[] arr4 = {11, 9, 0, 1};
        int[] arr5 = {2};

        Main main = new Main();
        main.showAnswer(arr1);
        main.showAnswer(arr2);
        main.showAnswer(arr3);
        main.showAnswer(arr4);
        main.showAnswer(arr5);

    }

    private int countEvens(int[] nums) {

        int count = 0;

        for (int num : nums) {
            if (num % 2 == 0) {
                count++;
            }
        }
        return count;
    }

    private void showAnswer(int[] array) {
        System.out.println(Arrays.toString(array) + " count of even numbers is " + countEvens(array));
    }
}
