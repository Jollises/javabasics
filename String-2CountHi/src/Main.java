public class Main {

    /*
    Return the number of times that the string "hi" appears anywhere in the given string.
    link: http://codingbat.com/prob/p147448
    */
    public static void main(String[] args) {
        Main main = new Main();
        main.showAnswer("");
        main.showAnswer("ABChi hi");
        main.showAnswer("hiHIhi");
        main.showAnswer("h");
        main.showAnswer("hi");
    }

    private int countHi(String str) {
        int count = 0;
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.substring(i, i + 2).equals("hi")) {
                count++;
            }
        }
        return count;
    }

    private void showAnswer(String str) {
        System.out.println(str + " | \"hi\" appears " + countHi(str) + " times");
    }
}
