import java.util.Arrays;

public class Main {

    /*
    Consider the leftmost and rightmost appearances of some value in an array. We'll say that the "span" is the number
    of elements between the two inclusive. A single value has a span of 1. Returns the largest span found in the given
    array. (Efficiency is not a priority.)
    link: http://codingbat.com/prob/p189576
    */
    public static void main(String[] args) {

        Main main = new Main();

        int[] arr1 = {1, 2, 1, 1, 3};
        int[] arr2 = {};
        int[] arr3 = {1};
        int[] arr4 = {3, 9};
        int[] arr5 = {1, 4, 2, 1, 4, 4, 4};
        int[] arr6 = {3, 3, 3};

        main.showAnswer(arr1);
        main.showAnswer(arr2);
        main.showAnswer(arr3);
        main.showAnswer(arr4);
        main.showAnswer(arr5);
        main.showAnswer(arr6);
    }

    private int maxSpan(int[] nums) {

        int maxSpan = 0;

        int tempSpan = 0;

        for (int i = 0; i < nums.length; i++) {

            int j = nums.length - 1; // j is the rightmost value

            // find the index of the same value of the rightmost element as the leftmost element
            while (nums[i] != nums[j]) {
                j--;
            }

            for (int k = i; k <= j; k++) { // k is the leftmost value that equals j
                tempSpan++; // count of elements between the same values
            }

            // finding max count of elements
            if (tempSpan > maxSpan) {
                maxSpan = tempSpan;
            }

            // reset tempSpan
            tempSpan = 0;
        }
        return maxSpan;
    }

    private void showAnswer(int[] array) {
        System.out.println(Arrays.toString(array) + " max span is " + maxSpan(array));
    }
}
